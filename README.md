# Pokémon GO Raid Bot
New version of [@pogoraidbot](https://t.me/pogoraidbot) written in [Laravel](https://laravel.com) with [BotMan](http://botman.io).
    
    
## Build
No build required, just install dependencies with:    

- `composer install`    
- `yarn install`    
    
    
## Test
Just create a local database and specify db-credentials in the `.env` file, then run `php artisan serve` and navigate to [http://127.0.0.1:8000/botman/tinker](http://127.0.0.1:8000/botman/tinker) to test the bot.
    
