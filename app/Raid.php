<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Raid extends Model
{
    protected $table = 'raids';

    public function user()
    {
        return $this->hasOne('App\User', 'user_id', 'user_id');
    }

    public function chat()
    {
        return $this->hasOne('App\Chat', 'chat_id', 'chat_id');
    }

    public function gym()
    {
        return $this->hasOne('App\Gym', 'id', 'gym_id');
    }

    public function pokemon()
    {
        return $this->hasOne('App\Pokemon', 'id', 'pokemon_id');
    }

    public function weather()
    {
        return $this->hasOne('App\Weather', 'id', 'weather_id');
    }

    public function attendances()
    {
        return $this->hasMany('App\Attendance', 'raid_id', null);
    }

    public function antiAttendances()
    {
        return $this->hasMany('App\AntiAttendance', 'raid_id', null);
    }
}
