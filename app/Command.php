<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Command extends Model
{
    protected $table = 'commands';

    protected $guarded = [
        'id',
    ];

    protected $fillable = [
        'command',
        'help',
    ];
}
