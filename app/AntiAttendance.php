<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AntiAttendance extends Model
{
	protected $table = 'anti_attendance';

    public function raid()
    {
        return $this->hasOne('App\Raid', 'id', 'raid_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'user_id', 'user_id');
    }
}
