<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
	protected $table = 'users';

	public function type()
	{
		return $this->belongsTo('App\UserGroup', 'id', 'user_group_id');
	}
}
