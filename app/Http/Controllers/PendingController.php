<?php

namespace App\Http\Controllers;


use App\Conversations\RaidCreateConversation;
use App\Pending;
use BotMan\BotMan\BotMan;

class PendingController extends Controller
{
	public function pendingCreate(BotMan $bot)
	{
		$pending = new Pending();
		$pending->type = 'create';
		$pending->save();

		$bot->startConversation(new RaidCreateConversation($pending));
	}
}