<?php

namespace App\Http\Controllers;


use App\Command;
use BotMan\BotMan\BotMan;

class HelpController extends Controller
{
	public function help(BotMan $bot, $cmd = null)
	{
		/** @var Command $command */
		if ($cmd) {
			$command = Command::where('command', $cmd)->first();

			if (!$command) {
				$bot->reply('Help per il comando <code>' . $cmd . '</code> non trovato!');
				return false;
			}
		} else
			$command = Command::where('command', 'help')->first();

		$bot->reply($command->help);
		return true;
	}
}
