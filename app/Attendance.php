<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $table = 'attendance';

    public function raid()
    {
        return $this->hasOne('App\Raid', 'id', 'raid_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'user_id', 'user_id');
    }

    public function chat()
    {
        return $this->hasOne('App\Chat', 'chat_id', 'chat_id');
    }
}
