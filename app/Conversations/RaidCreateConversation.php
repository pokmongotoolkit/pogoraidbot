<?php

namespace App\Conversations;

use App\Pending;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;

class RaidCreateConversation extends Conversation
{
	private $pending;

	public function __construct(Pending $pending)
	{
		$this->pending = $pending;
	}

	/**
	 * First question
	 */
	public function raidCreate(Pending $pending)
	{
		$question = Question::create(' iniziamo! Ho generato un link monouso per il tuo sondaggio Raid, procedi con il tasto CREA.')
			->fallback('Unable to ask question')
			->callbackId('raid_create')
			->addButtons([
				Button::create('CREA')->value('create'),
				Button::create('ANNULLA')->value('abort'),
			]);

		return $this->ask($question, function (Answer $answer) use ($pending) {
			if ($answer->isInteractiveMessageReply()) {
				if ($answer->getValue() === 'abort') {
					$this-
					$pending->delete();
				} else {
					redirect()->away('https://www.pokemongotoolkit.com');
				}
			}
		});
	}

	/**
	 * Start the conversation
	 */
	public function run()
	{
		$this->raidCreate($this->pending);
	}
}
