<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pending extends Model
{
    protected $table = 'pending';

    public function creator()
    {
        return $this->hasOne('App\User', 'user_id', 'creator_id');
    }

    public function chat()
    {
        return $this->hasOne('App\Chat', 'chat_id', 'chat_id');
    }

    public function raid()
    {
        return $this->hasOne('App\Raid', 'id', 'raid_id');
    }
}
