<?php

use App\Http\Controllers\BotManController;
use App\Http\Controllers\HelpController;
use App\Http\Controllers\PendingController;
use BotMan\BotMan\BotMan;

$botman = resolve('botman');

$botman->hears('test', function (BotMan $bot) {
    $bot->reply('Hello!');
});
$botman->hears('Start conversation', BotManController::class . '@startConversation');

/* Basic commands */
$botman->hears('/start', BotManController::class . '@start');
$botman->hears('/help', HelpController::class . '@help');
$botman->hears('/help {command}', HelpController::class . '@help');

/* Raid commands */
$botman->hears('/raid', PendingController::class . '@pendingCreate');
