<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance', function (Blueprint $table) {
			$table->charset = 'utf8';
			$table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->bigInteger('user_id')->nullable();
            $table->string('chat_id');
            $table->unsignedInteger('raid_id');
            $table->integer('extra_people')->nullable();
            $table->boolean('fly')->default(false);
            $table->timestamps();

			$table->foreign('user_id')->references('user_id')->on('users')->onUpdate('cascade')->onDelete('set null');
			$table->foreign('chat_id')->references('chat_id')->on('chats')->onUpdate('cascade')->onDelete('restrict');
			$table->foreign('raid_id')->references('id')->on('raids')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance');
    }
}
