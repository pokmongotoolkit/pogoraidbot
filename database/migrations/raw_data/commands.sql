# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 195.110.58.197 (MySQL 5.7.23-0ubuntu0.16.04.1)
# Database: pokemongotoolkit
# Generation Time: 2018-07-31 15:24:04 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table commands
# ------------------------------------------------------------

LOCK TABLES `commands` WRITE;
/*!40000 ALTER TABLE `commands` DISABLE KEYS */;

INSERT INTO `commands` (`id`, `command`, `help`, `ok_private`, `ok_public`, `ko_private`, `ko_public`)
VALUES
	(111,'d','<b>D</b>\r\n\r\nIl comando <code>/d</code> permette di eliminare un Raid.\r\n\r\n<b>UTILIZZO</b>\r\n/d<code> id_raid</code>\r\n\r\n<code>id_raid</code> è il codice identificativo del Raid stesso ed è riportato in fondo al messaggio del sondaggio relativo.\r\n\r\n<b>NOTE</b>\r\nUn Raid è eliminabile soltanto dagli amministratori e dal creatore del Raid stesso.\r\n\r\n<b>ATTENZIONE!</b> Questo comando elimina un sondaggio solo se questo non è stato condiviso in altre chat.',NULL,NULL,NULL,', devi dirmi il raid che vuoi eliminare con <code>/d id_raid</code>.\r\nPer maggiori informazioni digita <code>/help d</code>.'),
	(121,'import_old','<b>IMPORT</b>\r\n\r\nIl comando <code>/import</code> permette di importare un Raid in un gruppo.\r\n\r\n<b>UTILIZZO</b>\r\n/import<code> id_raid</code>\r\n\r\n<code>id_raid</code> è il codice identificativo del Raid stesso ed è riportato in fondo al messaggio del sondaggio relativo.\r\n\r\n<b>NOTE</b>\r\nÈ possibile importare un Raid solo se nel gruppo destinazione è presente questo bot.',NULL,NULL,NULL,', devi dirmi il raid che vuoi importare con <code>/import id_raid</code>.'),
	(131,'utenti_old','<b>UTENTI</b>\r\n\r\nIl comando <code>/utenti</code> permette di ottenere la lista di username degli iscritti ad un Raid (utile per contattarli in privato o menzionarli).\r\n\r\n<b>UTILIZZO</b>\r\n/utenti<code> id_raid</code>\r\n\r\n<code>id_raid</code> è il codice identificativo del Raid stesso ed è riportato in fondo al messaggio del sondaggio relativo.',NULL,NULL,NULL,', devi dirmi da che raid estrarre gli utenti <code>/utenti id_raid</code>.\r\nPer maggiori informazioni digita <code>/help utenti</code>.'),
	(141,'ansia','<b>ANSIA</b>\r\n\r\nIl comando <code>/ansia</code> permette di notificare tutti gli iscritti ad un Raid.\r\n\r\n<b>UTILIZZO</b>\r\n/ansia<code> id_raid</code>\r\n\r\n<code>id_raid</code> è il codice identificativo del Raid stesso ed è riportato in fondo al messaggio del sondaggio relativo.\r\n\r\n<b>NOTE</b>\r\nDi default questo comando è disponibile solo agli Amministratori. È possibile renderlo disponibile a tutti tramite il comando /settings.\r\n\r\nUSARE CON DISCREZIONE!',NULL,NULL,NULL,', devi dirmi da che raid estrarre gli utenti <code>/ansia id_raid</code>.\r\nPer maggiori informazioni digita <code>/help ansia</code>.'),
	(191,'settings','<b>IMPOSTAZIONI BOT</b>\r\nImpostazioni attuali del bot:\r\n\r\n',NULL,NULL,NULL,', solo gli amministratori hanno accesso alle impostazioni del bot.'),
	(193,'list',NULL,NULL,NULL,NULL,', non posso scrivere troppo in questa chat. <a href=\"https://t.me/pogoraidbot\">Clicca qui</a> e invia il comando al bot.'),
	(194,'help','<b>Pokémon GO Raid bot</b>\r\n\r\nBenvenuto allenatore!\r\nQuesto bot permette di creare sondaggi per i Raid di Pokémon GO in pochi semplici passaggi, semplificando moltissimo l\'organizzazione in gruppi per sconfiggere i Boss.\r\n\r\n<b>ISTRUZIONI</b>\r\n- digita /raid per iniziare\r\n- segui il link generato e completa il form con le informazioni richieste\r\n\r\n<em>Made with love by </em>@aroblu94','<b>COMANDI</b>\r\n\r\n<b>Creazione e importazione Raid</b>\r\n- /raid:	è il punto di partenza per la creazione di un Raid.\r\n- /exraid:	crea un multi-sondaggio per un Raid EX (disponibile solo agli admin).\r\n- /legendaryraid:	crea di creare un multi-sondaggio per un Raid Leggendario.\r\n\r\n<b>Modifica e eliminazione Raid</b>\r\n- Tocca il tasto con l\'ingranaggio per modificare un Raid.\r\n- /detach<code> id_raid</code>:	rimuove il Raid selezionato dalla chat (disponibile solo agli admin ed al creatore del raid).\r\n- /d<code> id_raid</code>:	elimina il Raid selezionato (disponibile solo agli admin ed al creatore del raid solo se non importato in altre chat).\r\n\r\n<b>Personali</b>\r\n- /level<code> livello</code>: imposta il livello giocatore.\r\n\r\n<b>Altro</b>\r\n- /ansia<code> id_raid</code>: notifica gli iscritti ad un Raid (disponibile solo agli admin).\r\n- /tag<code> id_raid messaggio</code>: notifica gli iscritti ad un Raid con un messaggio personalizzato.\r\n- /settings:	permette di modificare alcune impostazioni del bot specifiche per i vari gruppi.\r\n- /help:	visualizza questo messaggio.\r\n- /help<code> comando</code>:	visualizza l\'help per il comando specificato.','<b>CONTATTI</b>\r\n\r\nSito ufficiale: https://www.pokemongotoolkit.com\r\n\r\nSviluppatore: @aroblu94\r\n\r\nGruppo di supporto: https://goo.gl/zieTMC\r\n\r\nOffrimi una birra! https://goo.gl/G4cRre','<b>NEI GRUPPI</b>\r\n\r\nPer poter sfruttare al massimo le potenzialità di questo bot è consigliato aggiungerlo ad un Supergruppo e promuoverlo ad Amministratore dello stesso.\r\n\r\nAttraverso il comando /settings sono inoltre disponibili le impostazioni relative al gruppo per alcune funzionalità.',', non posso scrivere troppo in questa chat. <a href=\"https://t.me/pogoraidbot\">Clicca qui</a> e invia il comando al bot.'),
	(196,'detach','<b>DETACH</b>\r\n\r\nIl comando <code>/detach</code> permette di scollegare da una chat un Raid precedentemente importato.\r\n\r\n<b>UTILIZZO</b>\r\n/detach<code> id_raid</code>\r\n\r\n<code>id_raid</code> è il codice identificativo del Raid stesso ed è riportato in fondo al messaggio del sondaggio relativo.\r\n\r\n<b>NOTE</b>\r\nUn Raid è scollegabile soltanto dagli amministratori.',NULL,NULL,NULL,', devi dirmi l\'id del raid che vuoi scollegare con <code>/detach id_raid</code>.'),
	(197,'exraid','<b>EXRAID</b>\r\n\r\nIl comando <code>/exraid</code> permette di creare un multi-sondaggio per un Raid EX.\r\n\r\n<b>UTILIZZO</b>\r\n/exraid\r\n\r\n<b>NOTE</b>\r\nGenera un link monouso per la pagina di creazione di un multi-sondaggio Raid EX. Verranno richiesti soltanto la data, l\'orario di inizio e la palestra del Raid (selezionabile solo dalle palestre EX). Il sistema genererà 3 sondaggi identici, uno per team (riportato come team che domina la palestra e nelle note, da abilitare attraverso /settings).\r\n\r\n<b>ATTENZIONE!</b> I sondaggi creati con questo comando non sono modificabili. \r\nQuesto comando è disponibile solo agli Amministratori.',NULL,NULL,NULL,NULL),
	(198,'legendaryraid','<b>LEGENDARYRAID</b>\r\n\r\nIl comando <code>/legendaryraid</code> permette di creare un multi-sondaggio per un Raid Leggendario.\r\n\r\n<b>UTILIZZO</b>\r\n/legendaryraid\r\n\r\n<b>NOTE</b>\r\nGenera un link monouso per la pagina di creazione di un multi-sondaggio Raid Leggendario. Sarà possibile specificare tutti i dettagli del raid eccetto il Pokémon. Il sistema genererà tanti sondaggi quanti rono i Raid Boss leggendari attualmente attivi.',NULL,NULL,NULL,NULL),
	(199,'raid','<b>RAID</b>\r\n\r\nIl comando <code>/raid</code> permette di creare un sondaggio Raid.\r\n\r\n<b>UTILIZZO</b>\r\n/raid\r\n\r\n<b>NOTE</b>\r\nGenera un link monouso per la pagina di creazione di un sondaggio Raid. Per ogni Raid si possono specificare il Boss, gli orari di termine e ritrovo, la palestra, il Team che la domina ed il meteo. Tutti i membri del gruppo possono votare per la propria presenza eventualmente multipla. Il messaggio viene aggiornato ad ogni nuovo voto in tutti i gruppi in cui è stato condiviso. Un sondaggio può essere aggiornato e riproposto manualment e modificato attraverso gli appositi tasti.',NULL,NULL,NULL,NULL),
	(200,'tag','<b>TAG</b>\r\n\r\nIl comando <code>/tag</code> permette di notificare tutti gli iscritti ad un Raid.\r\n\r\n<b>UTILIZZO</b>\r\n/tag<code> id_raid messaggio</code>\r\n\r\n<code>id_raid</code> è il codice identificativo del Raid stesso ed è riportato in fondo al messaggio del sondaggio relativo.\r\n\r\n<code>messaggio</code> è il messaggio che verrà inviato notificando gli iscritti al Raid.\r\n\r\n<b>NOTE</b>\r\nDi default questo comando è disponibile solo agli Amministratori. È possibile renderlo disponibile a tutti tramite il comando /settings (gestito dall\'impostazione <code>tutti possono modificare</code>).',NULL,NULL,NULL,', devi dirmi da che raid estrarre gli utenti ed il messaggio da inviare con <code>/tag id_raid messaggio</code>.\r\nPer maggiori informazioni digita <code>/help tag</code>.'),
	(201,'level','<b>LEVEL</b>\r\n\r\nIl comando <code>/level</code> permette di modificare il proprio livello giocatore.\r\n\r\n<b>UTILIZZO</b>\r\n/level<code> livello</code>\r\n\r\n<code>livello</code> è il livello giocatore che verrà mostrato nei sondaggi Raid. Deve essere un numero intero compreso tra 1 e 40.\r\n\r\n<b>NOTE</b>\r\nQuesto comando funziona solo in privato.',NULL,NULL,NULL,', devi dirmi il livello giocatore da impostare con <code>/level livello</code>.\r\nPer maggiori informazioni digita <code>/help livello</code>.');

/*!40000 ALTER TABLE `commands` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
