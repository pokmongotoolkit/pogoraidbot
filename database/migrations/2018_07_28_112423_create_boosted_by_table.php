<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoostedByTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boosted_by', function (Blueprint $table) {
			$table->charset = 'utf8';
			$table->collation = 'utf8_general_ci';

            $table->unsignedInteger('pokemon_id');
            $table->unsignedInteger('weather_id');
            $table->timestamps();

            $table->foreign('pokemon_id')->references('id')->on('pokemons')->onUpdate('cascade')->onDelete('cascade');
			$table->foreign('weather_id')->references('id')->on('weather')->onUpdate('cascade')->onDelete('cascade');
        });

        $this->populate();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boosted_by');
    }

    private function populate() {
        $entities = json_decode(file_get_contents(__DIR__ . '/raw_data/boosted_by.json'), true)['RECORDS'];

        foreach ($entities as $entity)
            DB::table('boosted_by')->insert($entity);
    }
}
