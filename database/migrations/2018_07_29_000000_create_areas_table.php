<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreasTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('areas', function (Blueprint $table) {
			$table->charset = 'utf8';
			$table->collation = 'utf8_general_ci';

			$table->increments('id');
			$table->string('name');
			$table->unsignedInteger('capo_area');
			$table->string('provincia')->default('');
			$table->timestamp('enabled');
			$table->unsignedInteger('province_id');
			$table->integer('max_users')->default(50);
			$table->timestamps();

			$table->foreign('province_id')->references('id')->on('provinces')->onUpdate('cascade')->onDelete('restrict');
			$table->foreign('capo_area')->references('id')->on('users')->onUpdate('cascade')->onDelete('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('areas');
	}
}
