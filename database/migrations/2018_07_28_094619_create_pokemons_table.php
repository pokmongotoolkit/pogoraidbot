<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePokemonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pokemons', function (Blueprint $table) {
			$table->charset = 'utf8';
			$table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->string('code');
            $table->string('name');
            $table->integer('number');
            $table->integer('level');
            $table->boolean('shiny')->default(false);
            $table->boolean('enabled')->default(true);
            $table->timestamps();

            $table->unique('code');
        });

        $this->populate();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pokemons');
    }

	private function populate() {
		$entities = json_decode(file_get_contents(__DIR__ . '/raw_data/pokemons.json'), true)['RECORDS'];

		foreach ($entities as $entity)
			DB::table('pokemons')->insert($entity);
	}
}
