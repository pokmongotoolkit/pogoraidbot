<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commands', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->string('command');
            $table->text('help')->nullable();
            $table->text('ok_private')->nullable();
            $table->text('ok_public')->nullable();
            $table->text('ko_private')->nullable();
            $table->text('ko_public')->nullable();
            $table->timestamps();

            $table->unique('command');
        });

        $this->populate();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commands');
    }

    private function populate() {
        DB::unprepared(file_get_contents( __DIR__ . '/raw_data/commands.sql'));
    }
}
