<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pending', function (Blueprint $table) {
			$table->charset = 'utf8';
			$table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->string('chat_id')->nullable();
            $table->integer('message_id')->nullable();
            $table->bigInteger('creator_id')->nullable();
            $table->unsignedInteger('raid_id')->nullable();
            $table->enum('type', ['create', 'edit', 'refresh', 'settings'])->nullable();
            $table->timestamps();

			$table->foreign('creator_id')->references('user_id')->on('users')->onUpdate('cascade')->onDelete('cascade');
			$table->foreign('chat_id')->references('chat_id')->on('chats')->onUpdate('cascade')->onDelete('cascade');
			$table->foreign('raid_id')->references('id')->on('raids')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pending');
    }
}
