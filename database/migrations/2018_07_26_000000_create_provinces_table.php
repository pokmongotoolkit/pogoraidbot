<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateProvincesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('provinces', function (Blueprint $table) {
			$table->charset = 'utf8';
			$table->collation = 'utf8_general_ci';

			$table->increments('id');
			$table->string('name');
			$table->string('tag', 2);
			$table->integer('cod_pro');
			$table->integer('cod_reg');
			$table->timestamp('last_scan')->nullable();
			$table->timestamps();
		});

		$this->populate();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('provinces');
	}

	private function populate() {
		$entities = json_decode(file_get_contents(__DIR__ . '/raw_data/province.json'), true)['RECORDS'];

		foreach ($entities as $entity)
			DB::table('provinces')->insert($entity);
	}
}
