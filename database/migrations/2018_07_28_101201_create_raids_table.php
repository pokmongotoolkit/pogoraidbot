<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRaidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('raids', function (Blueprint $table) {
			$table->charset = 'utf8';
			$table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->bigInteger('user_id')->nullable();
            $table->string('chat_id')->nullable();
            $table->integer('level');
            $table->unsignedInteger('pokemon_id')->nullable();
            $table->unsignedInteger('weather_id')->nullable();
            $table->unsignedInteger('gym_id')->nullable();
            $table->string('lat')->comment('Ridondante');
            $table->string('lon')->comment('Ridondante');
            $table->string('gym_name')->comment('Ridondante');
            $table->enum('gym_team', ['instinct', 'mystic', 'valor', 'empty'])->nullable();
            $table->string('address')->comment('Ridondante');

            $table->string('timezone')->default('Europe/Rome');
            $table->timestamp('first_seen');
            $table->timestamp('meeting_time')->nullable();
            $table->timestamp('meeting_time_2')->nullable();
            $table->timestamp('end_time')->nullable();

            $table->integer('per_lobby')->default(0);
            $table->text('note')->nullable();
            $table->boolean('deleted')->default(false);
            $table->text('messages_ref')->nullable();
            $table->timestamps();

			$table->foreign('user_id')->references('user_id')->on('users')->onUpdate('cascade')->onDelete('set null');
			$table->foreign('chat_id')->references('chat_id')->on('chats')->onUpdate('cascade')->onDelete('set null');
			$table->foreign('gym_id')->references('id')->on('gyms')->onUpdate('cascade')->onDelete('set null');
			$table->foreign('pokemon_id')->references('id')->on('pokemons')->onUpdate('cascade')->onDelete('set null');
			$table->foreign('weather_id')->references('id')->on('weather')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raids');
    }
}
