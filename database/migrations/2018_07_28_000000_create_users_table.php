<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
			$table->charset = 'utf8';
			$table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->bigInteger('user_id');
            $table->string('nick')->nullable();
            $table->string('pogo_nick')->nullable();
            $table->string('name');
            $table->integer('level')->nullable();
            $table->enum('team', ['instinct', 'mystic', 'valor'])->nullable();
            $table->unsignedInteger('area')->nullable();
            $table->string('map_style')->default('base');
            $table->unsignedInteger('user_group_id')->default(4);
            $table->boolean('can_vote')->default(true);
            $table->boolean('can_vote_multiple')->default(true);
            $table->boolean('can_refresh')->default(true);
            $table->boolean('can_create')->default(true);
            $table->boolean('can_use_commands')->default(true);
            $table->timestamp('last_access')->nullable();
			$table->timestamps();

			$table->unique('user_id');
			$table->foreign('user_group_id')->references('id')->on('users_groups')->onUpdate('cascade')->onDelete('restrict');
			// TODO: foreign area -> areas.id
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
