<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUsersGroupsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_groups', function (Blueprint $table) {
			$table->charset = 'utf8';
			$table->collation = 'utf8_general_ci';

			$table->increments('id');
			$table->string('name');
			$table->string('note')->nullable();
			$table->timestamps();
		});

		$this->populate();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users_groups');
	}

	private function populate()
	{
		$entities = [
			[
				'id' => 1,
				'name' => 'Super Admin',
			],
			[
				'id' => 2,
				'name' => 'Admin',
			],
			[
				'id' => 3,
				'name' => 'Trusted',
			],
			[
				'id' => 4,
				'name' => 'Standard',
			],
		];

		foreach ($entities as $entity)
			DB::table('users_groups')->insert($entity);
	}
}
