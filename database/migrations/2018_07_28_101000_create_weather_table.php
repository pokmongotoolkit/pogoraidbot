<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeatherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather', function (Blueprint $table) {
			$table->charset = 'utf8';
			$table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->string('code');
            $table->string('name');
            $table->boolean('enabled')->default(true);
            $table->timestamps();

			$table->unique('code');
        });

        $this->populate();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather');
    }

	private function populate()
	{
		$entities = [
			[
				'id' => 1,
				'name' => 'Soleggiato',
				'code' => 'sunny',
			],
			[
				'id' => 11,
				'name' => 'Parz. Nuvoloso',
				'code' => 'partly_cloudy',
			],
			[
				'id' => 21,
				'name' => 'Coperto',
				'code' => 'cloudy',
			],
			[
				'id' => 31,
				'name' => 'Vento',
				'code' => 'windy',
			],
			[
				'id' => 41,
				'name' => 'Pioggia',
				'code' => 'rainy',
			],
			[
				'id' => 51,
				'name' => 'Nebbia',
				'code' => 'foggy',
			],
			[
				'id' => 61,
				'name' => 'Neve',
				'code' => 'snowy',
			],
			[
				'id' => 71,
				'name' => 'Cond. Estreme',
				'code' => 'extreme',
			],
		];

		foreach ($entities as $entity)
			DB::table('weather')->insert($entity);
	}
}
